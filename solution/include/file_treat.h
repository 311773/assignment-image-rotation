#ifndef FILE_TREAT_
#define FILE_TREAT_

#include <stdio.h>
#include "image.h"

enum read_status  {
  READ_OK = 0,
  READ_ERROR,
  READ_SIGNATURE_ERROR,
  READ_BITS_ERROR,
  READ_HEADER_ERROR
};

enum read_status from_bmp(FILE *in, struct image* img);

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_HEADER_ERROR,
  WRITE_BITS_ERROR
};

enum write_status to_bmp(FILE *out, struct image const* img);

#endif
