#ifndef IMG_TREAT_H_
#define IMG_TREAT_H_

#include <stdbool.h>
#include "image.h"
#include <inttypes.h>


struct image img_create(uint64_t width, uint64_t height);

void img_destroy(struct image* img);

#endif
