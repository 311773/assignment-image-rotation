#ifndef IMG_ROTATE_
#define IMG_ROTATE_

#include <stdbool.h>
#include "image.h"


bool rotate(struct image* img);

#endif
