#include "file_treat.h"
#include "bmp_header.h"
#include "img_treat.h"
#include <stdbool.h>
#include <stdlib.h>

#define BF_TYPE 19778
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_X_PELS_PER_METER 0
#define BI_Y_PELS_PER_METER 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0

static uint8_t padding(uint64_t width) {
    return 4 - (width * sizeof(struct pixel)) % 4;
}

static struct bmp_header create_bmp_header(uint32_t width, uint32_t height) {
    uint32_t size_image = height * (width * 3 + padding(width));
    return (struct bmp_header) {
        .bfType = BF_TYPE,
        .bfReserved = BF_RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BI_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = BI_PLANES,
        .biBitCount = BI_BIT_COUNT,
        .biCompression = BI_COMPRESSION,
        .biSizeImage = size_image,
        .bfileSize = size_image + sizeof (struct bmp_header),
        .biXPelsPerMeter = BI_X_PELS_PER_METER,
        .biYPelsPerMeter = BI_Y_PELS_PER_METER,
        .biClrUsed = BI_CLR_USED,
        .biClrImportant = BI_CLR_IMPORTANT,
    };
}

static bool arg_err() {
    fprintf(stderr, "Invalid arguments");
    return false;
}

static bool img_to_array(struct image const* img, uint8_t* byte_array) {
    if (img == NULL || byte_array == NULL)
        return arg_err();
    size_t pointer = 0;
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            byte_array[pointer] = (img->data)[i*img->width+j].b;
            byte_array[pointer + 1] = (img->data)[i*img->width+j].g;
            byte_array[pointer + 2] = (img->data)[i*img->width+j].r;
            pointer += 3;
        }
        for (size_t k = 0; k < padding(img->width); k++) {
            byte_array[pointer] = 0;
            pointer++;
        }
    }
    return true;
}

static bool array_to_img(struct image* img, uint8_t const* byte_array) {
    if (img == NULL || byte_array == NULL) {
        return arg_err();
    }
    size_t pointer = 0;
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            (img->data)[i*img->width+j].b = byte_array[pointer];
            (img->data)[i*img->width+j].g = byte_array[pointer + 1];
            (img->data)[i*img->width+j].r = byte_array[pointer + 2];
            pointer += 3;
        }
        pointer += padding(img->width);
    }
    return true;
}

enum read_status from_bmp(FILE *in, struct image* img) {
    if (in == NULL || img == NULL) {
        arg_err();
        return READ_ERROR;
    }

    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        fprintf(stderr, "Header reading error");
        return READ_HEADER_ERROR;
    }
    if (header.biBitCount != 24) {
        fprintf(stderr, "Only 24bit bmp");
        return READ_SIGNATURE_ERROR;
    }
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    if (img->data == NULL) {
        fprintf(stderr, "Memory allocation for data failed");
        return READ_ERROR;
    }

    size_t size_array = (img->width * 3 + padding(img->width)) * img->height;
    uint8_t* byte_array = malloc(sizeof(uint8_t) * size_array);
    if (byte_array == NULL) {
        free(img->data);
        fprintf(stderr, "Memory allocation for array failed");
        return READ_ERROR;
    }

    if (fread(byte_array, sizeof(uint8_t), size_array, in) != size_array) {
        free(img->data);
        free(byte_array);
        fprintf(stderr, "Array reading error");
        return READ_BITS_ERROR;
    }

    if (!array_to_img(img, byte_array)) {
        free(img->data);
        free(byte_array);
        return READ_ERROR;
    }
    free(byte_array);
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const* img) {
    if (out == NULL || img == NULL) {
        arg_err();
        return WRITE_ERROR;
    }

    struct bmp_header header = create_bmp_header(img->width, img->height);
    size_t size_array = (img->width * 3 + padding(img->width)) * img->height;
    uint8_t* byte_array = malloc(sizeof(uint8_t) * size_array);
    if (byte_array == NULL) {
        fprintf(stderr, "Memory allocation for array failed");
        return WRITE_ERROR;
    }
    if (!img_to_array(img, byte_array)) {
        free(byte_array);
        return WRITE_HEADER_ERROR;
    }
    if (fwrite(&header, header.bOffBits, 1, out) != 1) {
        free(byte_array);
        fprintf(stderr, "Header writing error");
        return WRITE_HEADER_ERROR;
    }
    if (fwrite(byte_array, sizeof(uint8_t), size_array, out) != size_array) {
        free(byte_array);
        fprintf(stderr, "Array writing error");
        return WRITE_BITS_ERROR;
    }
    free(byte_array);
    return WRITE_OK;
}
