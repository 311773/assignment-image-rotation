#include "file_treat.h"
#include "img_rotate.h"
#include "img_treat.h"
#include <inttypes.h>
#include <stdio.h>


int main(int argc, char** argv) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    
    if (argc != 3) {
        fprintf(stderr, "Invalid arguments\n");
        return 1;
    }

    FILE* in = fopen(argv[1], "rb");
    if (in == NULL) {
        fprintf(stderr, "Input file error\n");
        return 1;
    }

    struct image img = {0};
    if (from_bmp(in, &img) != READ_OK){
	fprintf(stderr, "Read error\n");
        img_destroy(&img);
        fclose(in);
        return 1;
    }
    fclose(in);

    if (!rotate(&img)) {
        img_destroy(&img);
        return 1;
    }

    FILE* out = fopen(argv[2], "wb");
    if (out == NULL) {
        img_destroy(&img);
        fprintf(stderr, "Output file error\n");
        return 1;
    }
    if (to_bmp(out, &img) != WRITE_OK) {
	fprintf(stderr, "Write error\n");
        img_destroy(&img);
        fclose(out);
        return 1;
    }
    fclose(out);
    img_destroy(&img);

    return 0;
}
