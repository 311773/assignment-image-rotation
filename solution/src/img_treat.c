#include "img_treat.h"
#include <stdio.h>
#include <stdlib.h>

struct image img_create(uint64_t width, uint64_t height){
    return ((struct image) {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    });
}

void img_destroy(struct image* img){
    if (img != NULL) {
        free(img->data);
    } else {
        fprintf(stderr, "Invalid arguments");
    }
}

